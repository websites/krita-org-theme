<div class="row">
    <div class="content-container col-md-8" >
    
        <!-- Nav tabs -->
        <ul id="downloadAreas" class="nav nav-tabs">
          <li class="active"><a href="#windows" data-toggle="tab">Windows Build</a></li>
          <li><a href="#linux" data-toggle="tab">GNU/Linux</a></li>
          <li><a href="#source" data-toggle="tab">Source Code</a></li>
        </ul>

        <!-- Tab content (is linked by id to nav) -->

        <div class="tab-content" >
        
          <!-- start windows tab -->
          <div class="tab-pane active" id="windows">
            <p class="description">Our Windows binaries are provided by GmbH. GmbH provides professional, paid support if you use it professionally
and need that type of service. Don't worry though, downloading and using Krita will always be free.</p>
            
            <img src="<?php echo bloginfo('template_directory')?>/images/windows-logo-reverse.png" alt="Windows Logo" />
            <div class="os-download-notes">
                <p>Compatible with Windows 7 | Vista | 8</p>
                <p><em>There is no Windows XP support.</em></p>
            </div>

           <a href="#" class="button"><img src="<?php echo bloginfo('template_directory')?>/images/download-icon.png" alt="download icon" />32-bit</a>
           <a href="#" class="button"><img src="<?php echo bloginfo('template_directory')?>/images/download-icon.png" alt="download icon" />64-bit</a>

          
            <div  style="clear:both"></div>
          </div>
          
          
          
          <!-- start Linux stable tab -->
          <div class="tab-pane" id="linux">
               <p class="description">Linux distributions package the currently stable version of Linux when they release a new version. Some distributions provide backports repositories which offer the latest stable version of Krita.</p>
                
                <img src="<?php echo bloginfo('template_directory')?>/images/linux-logo-reverse.png" alt="linux Logo" />
                <div class="os-download-notes">
                    <p>Need to review if what is on the page is current</p>
                </div>

                <div  style="clear:both"></div>
          </div>

        <div class="tab-pane" id="source">
             <p class="description">There are a few different ways to download and build Krita yourself. All of our repositories and build documentation are on KDE. Visit the <a href="#" >Building Calligra guide</a> to find out the best way to download the various versions of the source.</p>
          </div>
        </div>

    </div>
  
</div>   

<div class="row">
    <div class="content-container" class="col-md-12">     
        
        <h2>Give us support</h2>

        <p>Help us to keep growing and give to you the best features! Donations to the Krita Foundation will be used in the following order of priority:</p>
        
        <ol>
            <li><strong>Fund development of Krita:</strong> The Foundation will sponsor a developer to work full-time on Krita for a specific period of time, for tasks including bug-fixing, adding new features and improving support on platforms other than Linux. Please find the results of such past initiatives here.</li>
            <li><strong>Hardware:</strong> The Krita Foundation may sometimes use funds to provide some developers with needed hardware such as Wacom tablets. This is only done if they need to work on coding for that hardware.</li>
            <li><strong>Fund travel to events:</strong> Such as the Libre Graphics Meeting and Development sprints. Krita will usually try to get funding for these from other sources first, like the event planners themselves.</li>
        </ol>

    </div>
</div>

<script>
    jQuery('#downloadAreas a').click(function (e) {
      e.preventDefault();
      jQuery(this).tab('show');
    });
</script>

<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>


<?php get_header(); ?>





<style>
/*  clip-path HAS to be defined inline like this for it to work in Firefox 49 (tested 9/25/2016) 
    no support for clip-path in IE, so they will have to settle for just a box
  */
#frontpage-slideshow {
   -webkit-clip-path: url("#clip-1"); 
   clip-path: url("#clip-1");
}

/* slide show stuff on homepage */
#frontpage-slideshow {   
    height:500px;
    background: url('https://krita.org/wp-content/themes/krita-org-theme/images/slideshow/hero-image-42.png');
    background-size: cover;
    background-position:center; 
}




/* Safari on OSX freaks out if clip-path is set, so explicitly set it to none.  */
#frontpage-slideshow.ie-decoration-fix {
   -webkit-clip-path: none; 
   clip-path: none;
}

</style>



<div class="row" id="frontpage-slideshow">
        <div id="hero" class="col-md-12">

      <!-- needs to be above the actual HTML for it to work -->
        <svg width="0" height="0">
          <defs>
            <clippath id="clip-1" clipPathUnits="objectBoundingBox">
              <polygon points="0 0, 1 0, 1 1, 0 .7" />
            </clippath>
          </defs>
        </svg>


            <a href="https://www.kde.org" id="kde-homepage-logo"><span style="display: inline-block;  -webkit-transform: rotate(-30deg);     -moz-transform: rotate(-30deg); 
                        width: 80px;
                        line-height: 1.3em;
                        background: #1A1B25;
                        color: white;
                        text-align: center;                    
                        padding: 1em;
                        border-radius: 4em;
                        margin-top: 3.8em;
                        margin-left: 2em;">
                        <img src="https://krita.org/wp-content/themes/krita-org-theme/images/kde-logo.png" height="50" width="50" />
            </a>

            <div id="slogan">
                <span style="display: inline-block;"><?php esc_html_e( 'Krita is a professional FREE and open source painting program. It is made by artists that want to see affordable art tools for everyone.', 'krita-org-theme' ); ?></span> 
                <ul style="margin-bottom: 0.7em">
                    <li><?php esc_html_e( 'concept art', 'krita-org-theme' ); ?></li>
                    <li><?php esc_html_e( 'texture and matte painters', 'krita-org-theme' ); ?></li>
                    <li><?php esc_html_e( 'illustrations and comics', 'krita-org-theme' ); ?></li>
                </ul>

                <!-- pll_get_post gets a webpage ID. You give it the English ID, and it returns the language ID that you are on -->
                <!-- it looks like dev uses the same ID as production, so we should be ok hard-coding the ID when going live -->
                <a href="<?php echo get_permalink( pll_get_post(586) )    ?>" class="pink-button" style="display:inline-block">
                    <?php esc_html_e( 'GET KRITA NOW', 'krita-org-theme' ); ?>
                </a>

                 <img class="supportedOSIcon" src="<?php echo bloginfo('template_directory')?>/images/decoration/os-supported-icons-all.png" height="50" width="110"  />

            </div>
        

        </div>    

</div>


<div class="row" id="header-decoration">
    <div class="col-md-12">&nbsp;</div>
</div>



<div id="slogan-mobile">
    <span style="display: inline-block;"><?php esc_html_e( 'Krita is a professional FREE and open source painting program. It is made by artists that want to see affordable art tools for everyone.', 'krita-org-theme' ); ?></span> 
    <ul style="margin-bottom: 0.7em">
        <li><?php esc_html_e( 'concept art', 'krita-org-theme' ); ?></li>
        <li><?php esc_html_e( 'texture and matte painters', 'krita-org-theme' ); ?></li>
        <li><?php esc_html_e( 'illustrations and comics', 'krita-org-theme' ); ?></li>
    </ul>

    <!-- pll_get_post gets a webpage ID. You give it the English ID, and it returns the language ID that you are on -->
    <!-- it looks like dev uses the same ID as production, so we should be ok hard-coding the ID when going live -->
    <a href="<?php echo get_permalink( pll_get_post(586) )    ?>" class="pink-button" style="display:inline-block"><?php esc_html_e( 'GET KRITA NOW', 'krita-org-theme' ); ?></a>

     <img class="supportedOSIcon" src="<?php echo bloginfo('template_directory')?>/images/decoration/os-supported-icons-all.png" height="50" width="120"  />

</div>



<!-- new content goes here -->

<div class="row" >
    <div class="col-md-8" >
        <h3><?php esc_html_e( 'Tools You Need to Grow as an Artist', 'krita-org-theme' ); ?></h3>

        <div class="row" >
            
<!-- <a href="<?php echo get_permalink( pll_get_post(586) )    ?>" class="pink-button" style="display:inline-block">GET KRITA NOW</a> -->

            <div class="col-md-6 fontpage-panel" >
                <a class="frontpage-toolsImage" href="<?php echo get_permalink( pll_get_post(580) ) ?>"  style="display:block">
                    <div class="text-info">
                        <span class="main-text underline"><?php esc_html_e( 'All the features you need', 'krita-org-theme' ); ?></span>
                        <span class="more-arrow pink-button"> &#9654; </span>
                    </div>  
                </a>
            </div>

            <div class="col-md-6" >
                <div class="row" >
                    <div class="col-md-12 fontpage-panel" >
                        <a class="frontpage-education" href="<?php echo get_permalink( pll_get_post(606) )    ?>" style="display:block"> 
                            <div class="text-info">
                                <span class="main-text underline"><?php esc_html_e( 'FREE education and resources', 'krita-org-theme' ); ?></span>
                                <span class="more-arrow pink-button"> &#9654; </span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12 fontpage-panel" >
                        <div class="frontpage-community">
                            <div class="text-info">
                                <span class="main-text">
                                    <?php esc_html_e( 'Supportive community.', 'krita-org-theme' ); ?> <a href="https://forum.kde.org/viewforum.php?f=136"><?php esc_html_e( 'A Friendly forum.', 'krita-org-theme') ?></a><br/>
                                     <div id="socialmedia" style="float: left" >                
                    
                                        <a href="http://krita-free-art-app.deviantart.com/" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-deviantArt']);><img src="<?php echo bloginfo('template_directory')?>/images/social-deviantart.png" alt="" /></a>
                                        <a href="https://www.facebook.com/pages/Krita-Foundation/511943358879536" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-facebook']);><img src="<?php echo bloginfo('template_directory')?>/images/social-facebook.png" alt="" /></a>
                                        
                                        <a href="https://twitter.com/Krita_Painting" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-twitter']);><img src="<?php echo bloginfo('template_directory')?>/images/social-twitter.png" alt="" /></a>
                                        <a href="https://vk.com/ilovefreeart" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-VK']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-vk.png" alt="" /></a> 
                                        <a href="https://www.reddit.com/r/krita/" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-reddit']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-reddit.png" alt="" /></a> 
                                        <a href="https://mastodon.art/@krita" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-mastodon']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-mastodon.png" alt="" /></a> 
                                    </div>
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    
    <div class="col-md-4" >
        
        <h3 style="display:inline-block">
            <a href="https://www.krita.org/<?php echo pll_current_language() ?>/?post_type=post&amp;s="><?php esc_html_e( 'News', 'krita-org-theme' ); ?></a>
        </h3>


        <a href="<?php bloginfo('rss2_url'); ?>" align="left" onclick="_paq.push(['trackEvent', 'FrontPage', 'button', 'RSS']);
        " ><img src="<?php echo bloginfo('template_directory')?>/images/rss-icon-blue.png" alt="rss-icon" /></a>


        <span id="font-search">
            <?php get_search_form(); ?>
        </span>

        

        <div class="row" >
            <div class="col-md-12 fontpage-panel" >
                <div class="front-page-news-container">
                <!-- start showing last 10 news posts -->
                <!-- get the last 10 post summaries here -->
                <?php
                
                query_posts('posts_per_page=4');
                if (have_posts() ) 
                {
                    while (have_posts())
                    {        
                        the_post();
                        
                        //set up date formatting
                        $full_date = get_the_date( $d );
                        
                     ?>
                    

                    <a href="<?php echo get_permalink();  ?>"><?php echo the_title()  ?></a>

                    <div><?php echo the_time(get_option( 'date_format' )) ?> </div>

                <?php
                    }
                } else {
                 ?>
 
                <div><?php esc_html_e( 'No results found', 'krita-org-theme' ); ?></div>

                <?php
                }
                ?>
                <!-- end showing last last 10 news posts -->
                </div>
            </div>
        </div>

    </div>
</div>


<!-- end new content -->









<div class="row" style="margin-top: 3.0em" >


<!-- begin showing artist interview area -->
<?php
            
              query_posts( array(
    'category_name'  => 'Artist Interview',
    'posts_per_page' => 1
) );

 if (have_posts() ) 
        {
            while (have_posts())
            {        
                the_post();                
?>


       
                 


                   
                    <?php if (has_post_thumbnail( $post->ID ) ): ?>
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>


    <div class="col-md-4 fontpage-panel" >
            <h3><?php esc_html_e( 'Artist Interview', 'krita-org-theme' ); ?></h3>            
        <a href="<?php echo get_permalink();  ?>">

            <div class="artist-interview-container" style="background-image: url('<?php echo $image[0]; ?>')">
                <div class="text-info">
                    <span class="main-text underline"><?php echo the_title()  ?></span>
                    <span class="more-arrow pink-button"> &#9654; </span>
                </div>
            </div>
        </a>

    </div>




<?php endif; 
     }   
   } else {

    // don't show anything if there is no artist interview
    echo '<!-- no artist interview to show  -->';

}

?>
<!-- end showing artist interview area -->

















    <div class="col-md-8 fontpage-panel" >
           
            <div class="row">
                <div class="col-md-12">
                    <h3><?php esc_html_e( 'Get Involved ', 'krita-org-theme' ); ?></h3>
                    <a class="frontpage-volunteer" href="<?php echo get_permalink( pll_get_post(614) )    ?>" style="display:block">
                        <div class="text-info">
                            <span class="main-text underline"><?php esc_html_e( 'Volunteer your skills and help make Krita better for everyone', 'krita-org-theme' ); ?></span>
                            <span class="more-arrow pink-button"> &#9654; </span>
                        </div>
                    </a>
                </div>
            </div>


            <div class="row" style="margin-top: 2.0em">
                <div class="col-md-12">

                    <h3><?php esc_html_e( 'Give Back', 'krita-org-theme' ); ?></h3>
                    <a class="frontpage-donate" href="<?php echo get_permalink( pll_get_post(630) )    ?>" style="display:block">
                        <div class="text-info">
                            <span class="main-text underline"><?php esc_html_e( 'A little money goes a long way', 'krita-org-theme' ); ?></span>
                            <span class="more-arrow pink-button"> &#9654; </span>
                        </div>
                    </a>


                </div>
            </div>


    </div>

</div>

    <div class="row" style="margin-top: 6rem; text-align: center" >
        <div class="col-md-12">
             <h2 style="color: grey; font-style: italic;">
<?php esc_html_e( '#1 Best free painting software in 2019', 'krita-org-theme' ); ?>
             </h2>
        </div>
        <div class="col-md-12">       
            <img style="border: 0; max-width: 300" src="https://krita.org/wp-content/uploads/2019/02/techradar-logo.gif" alt="Techradar logo" />
        </div>
    </div>



<!-- <div class="row">
    
</div> -->

<div class="row testimonial" style="padding: 3rem 2rem; text-align: center; margin: 4rem -0.8rem;">
    <div class="col-md-12">
        <img style="border: 0; max-width: 250px; margin: 0 auto" src="https://krita.org/wp-content/uploads/2019/02/5-star.png" />
        <p class="quote">...<?php esc_html_e( 'it is clear that Krita is not half-baked or an amateur home project: indeed it looks and feels very professional', 'krita-org-theme' ); ?>

        </p>
        <img style="border: 0; max-width: 330px; min-width: 30px; margin: 0 auto" src="https://krita.org/wp-content/uploads/2019/02/imaginefx-logo.png" alt="ImagineFX logo" />
    </div>

</div>




<?php get_template_part('email-signup-snippet', 'index'); ?>



<?php get_footer(); ?>

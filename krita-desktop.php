<?php
/**
 * Template Name: Krita Desktop Page
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>



<?php
// https://stackoverflow.com/questions/2602612/php-remote-file-size-without-downloading-file
/**
 * Returns the size of a file without downloading it, or -1 if the file
 * size could not be determined.
 *
 * @param $url - The location of the remote file to download. Cannot
 * be null or empty.
 *
 * @return The size of the file referenced by $url, or -1 if the size
 * could not be determined.
 */
function curl_get_file_size( $url ) {
  // Assume failure.
  $result = -1;

  $curl = curl_init( $url );

  // Issue a HEAD request and follow any redirects.
  curl_setopt( $curl, CURLOPT_NOBODY, true );
  curl_setopt( $curl, CURLOPT_HEADER, true );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
 // curl_setopt( $curl, CURLOPT_USERAGENT, get_user_agent_string() ); // giving error


  $data = curl_exec( $curl );
  curl_close( $curl );

  if( $data ) {
    $content_length = "unknown";
    $status = "unknown";

    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
      $status = (int)$matches[1];
    }

    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
      $content_length = (int)$matches[1];
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if( $status == 200 || ($status > 300 && $status <= 308) ) {
      $result = $content_length;
    }
  }


  $resultMegaBytes = $result/1024/1024;

  if ($resultMegaBytes > 0) {

    if (round($resultMegaBytes) < 10) {
      return round($resultMegaBytes, 1); // less than 10 MB
    } else {
      return round($resultMegaBytes); // more than 10 MB
    }

    
  } else {
    return "???";
  }


 // return round($result/1024/1024); // returns in megabytes
}
?>




<?php
  // get the file size of the download link (from a HEADER request)
  $windowssixtyFourInstallerSize = curl_get_file_size( get_post_meta(592, "Windows 64-bit Installer URL", true)); 
  $windowsthirtyTwoInstallerSize = curl_get_file_size( get_post_meta(592, "Windows 32-bit Installer URL", true)); 
  $osxDmgSize = curl_get_file_size( get_post_meta(592, "OSX DMG URL", true)); 
  $linuxAppImageSize = curl_get_file_size( get_post_meta(592, "Appimage 64-bit URL", true)); 

  $windowsSixtyFourPortableSize = curl_get_file_size( get_post_meta(592, "Windows 64-bit Portable URL", true) );
  $windowsThirtyTwoPortableSize = curl_get_file_size( get_post_meta(592, "Windows 32-bit Portable URL", true) );

  $windowsShellExtensionsSize = curl_get_file_size( get_post_meta(592, "Windows Shell Extension URL", true) );
  $tarballSize = curl_get_file_size( get_post_meta(592, "Tarball URL", true) );
?>


<?php get_header(); ?>


<div class="row" id="download-page-hero">
  <div id="download-header-inside" class="col-md-12">
      <div id="download-content-container">
        <h3><?php esc_html_e( 'Download Krita', 'krita-org-theme' ); ?> 
          <?php echo get_post_meta(592, "Version Number", true)    ?></h3>
        <p><?php esc_html_e( 'Released on', 'krita-org-theme' ) ?> 

          <!-- the "Release Date" WordPress variable needs to be in MM/DD/YYYY  -->
          <?php  
           // take the release date and localize it since different locales format it differently
           // the Release Date variable needs to be in MM/DD/YYY format
            $currentPeriod = date_i18n('F d, Y', strtotime(get_post_meta(592, "Release Date", true))) ;   
            echo $currentPeriod;
          ?>

          <!--  Each language will need to specifify a release notes. If a translation doesn't exist, the link will not appear   -->
          <?php 
           $releaseNotesURL = sprintf( get_post_meta(get_the_ID(), "Release Notes URL (type 'false' if none)", true) );

          if ( $releaseNotesURL != "false" && $releaseNotesURL != "none"   ) { ?>
              <a href="<?php echo $releaseNotesURL  ?>"> <strong><?php esc_html_e( 'Release Notes', 'krita-org-theme' ) ?></strong></a>
          <?php } ?>
        </p>
        
        <span id="windows-download-information" style="display:none"> <!-- 1515 is english download page on dev... 592 is on production -->
            <a id="sixty-four-bit-windows-installer" href="<?php echo get_post_meta(592, "Windows 64-bit Installer URL", true)  ?>" class="pink-button download-trigger" style="display:none">
              <img src="<?php echo bloginfo('template_directory')?>/images/decoration/download-icon-windows.png" />
              <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '64-bit', 'krita-org-theme' ) ?>  (<?php echo $windowssixtyFourInstallerSize . 'MB'; ?>)
            </a>
            <a id="thirty-two-bit-windows-installer" href="<?php echo get_post_meta(592, "Windows 32-bit Installer URL", true)  ?>" class="pink-button download-trigger" style="display:none">
                <img src="<?php echo bloginfo('template_directory')?>/images/decoration/download-icon-windows.png" /> 
                <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?> 
                <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?> 
                <?php esc_html_e( '32-bit', 'krita-org-theme' ); ?> (<?php echo $windowsthirtyTwoInstallerSize . 'MB'; ?>)  </a>
        </span>

        <span id="osx-download-information" style="display:none">
              <a href="<?php echo get_post_meta(592, "OSX DMG URL", true)  ?>" class="pink-button download-trigger" style="display:inline-block; margin-bottom: 0.5em;">
                <img src="<?php echo bloginfo('template_directory')?>/images/decoration/download-icon-osx.png" />
                <?php esc_html_e( 'Mac OSX ', 'krita-org-theme' ); ?> 
                <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?>  (<?php echo $osxDmgSize . 'MB'; ?>)
              </a>
              <p><?php esc_html_e( 'Krita on OSX does not contain G\'Mic or the touch docker right now.', 'krita-org-theme' ); ?></p>
        </span>

        <span id="linux-download-information" style="display:none">

            <!-- a few variations for linux displayed in tabs -->
            <ul class="nav nav-tabs" style="display:inline-block">
              <li class="active"><a data-toggle="tab" href="#appImageTab"><?php esc_html_e( 'Appimage', 'krita-org-theme' ); ?></a></li>
              <li><a data-toggle="tab" href="#ubuntuTab"><?php esc_html_e( 'Ubuntu PPA', 'krita-org-theme' ); ?></a></li>
              <li><a data-toggle="tab" href="#flatpakTab"><?php esc_html_e( 'Flatpak', 'krita-org-theme' ); ?></a></li>
              <li><a data-toggle="tab" href="#gentooTab"><?php esc_html_e( 'Gentoo', 'krita-org-theme' ); ?></a></li>
            </ul>

            <div class="tab-content">
            
              <div id="appImageTab" class="tab-pane fade in active">
                <a href="<?php echo get_post_meta(592, "Appimage 64-bit URL", true)  ?>" class="pink-button download-trigger piwik_download" style="display:inline-block; margin-bottom: 0.5em;">
                  <img src="<?php echo bloginfo('template_directory')?>/images/decoration/download-icon-linux.png" /> 
                  <?php esc_html_e( 'Linux', 'krita-org-theme' ); ?>
                  <?php esc_html_e( '64-bit', 'krita-org-theme' ); ?> 
                  <?php esc_html_e( 'Appimage', 'krita-org-theme' ); ?> (<?php echo $linuxAppImageSize . 'MB'; ?>) </a>
                <p>
                  <?php esc_html_e( 'Get the latest version without relying on your distro.', 'krita-org-theme' ); ?>
                  <?php esc_html_e( 'This version does not support sound when doing animations.', 'krita-org-theme' ); ?>
                  <a href="https://download.kde.org/stable/krita/<?php echo get_post_meta(592, "Version Number", true)    ?>/gmic_krita_qt-x86_64.appimage">
                    <?php esc_html_e( 'Download the gmic-qt plugin for G\'Mic to work.', 'krita-org-theme' ); ?></a>
                </p>
                 
              </div>
              <div id="ubuntuTab" class="tab-pane fade in">
                <p><code>https://launchpad.net/~kritalime/+archive/ubuntu/ppa</code></p>
                <p>
                  <?php esc_html_e( 'Works on any Ubuntu derivative: Linux Mint, Elementary OS, etc.', 'krita-org-theme' ); ?>
                   <a href="https://download.kde.org/stable/krita/<?php echo get_post_meta(592, "Version Number", true)    ?>/gmic_krita_qt-x86_64.appimage" class="piwik_download">
                    <?php esc_html_e( 'Download the gmic-qt plugin for G\'Mic to work.', 'krita-org-theme' ); ?>
                  </a>
                </p>
              </div>
              <div id="flatpakTab" class="tab-pane fade in">
                 
<!--
                 <p><?php esc_html_e( 'Install', 'krita-org-theme' ); ?>: <code>flatpak remote-add --if-not-exists flathub<br/>
                  flatpak install flathub org.kde.krita</code></p>
                  <p><?php esc_html_e( 'Run', 'krita-org-theme' ); ?>: <code>flatpak run org.kde.krita</code></p>
-->

                  <p>
                    <?php esc_html_e( 'Maintained by the community.', 'krita-org-theme' ); ?>
                    <a href="https://flathub.org/apps/details/org.kde.krita"><?php esc_html_e( 'Hosted on Flathub.', 'krita-org-theme' ); ?></a>
                    </p>
                    <p>
                     <a href="https://download.kde.org/stable/krita/<?php echo get_post_meta(592, "Version Number", true)    ?>/gmic_krita_qt-x86_64.appimage" class="piwik_download">
                      <?php esc_html_e( 'Download the gmic-qt plugin for G\'Mic to work.', 'krita-org-theme' ); ?>
                    </a>
                  </p>
              </div>
              <div id="gentooTab" class="tab-pane fade in">
                <p><code>layman -a bloody && emerge --sync && emerge krita</code></p>
                <p><?php esc_html_e( 'Maintained by community.', 'krita-org-theme' ); ?>
                  <a href="https://download.kde.org/stable/krita/<?php echo get_post_meta(592, "Version Number", true)    ?>/gmic_krita_qt-x86_64.appimage" class="piwik_download">
                    <?php esc_html_e( 'Download the gmic-qt plugin for G\'Mic to work.', 'krita-org-theme' ); ?>
                  </a>
                </p>
              </div>
          </div>


        </span>

        <?php // system requirements are in one string, so construct it here and make it translatable
              // notice the "__" for the translation. This is needed when assigning translated strings to a variable 
          $systemRequirements =  '<strong>' . 
             __( 'Operating System', 'krita-org-theme' ) .  '</strong>'  .
             __( ':', 'krita-org-theme' ) .
             __( 'Windows 8.1 or Higher, OSX 10.12, Linux', 'krita-org-theme' ) . "<br/><strong>" . 
             __( 'RAM', 'krita-org-theme' ) . "</strong>" .
             __( ':', 'krita-org-theme' ) .
             __( 'Recommended 4GB or higher', 'krita-org-theme' ) . " <br/><strong>" . 
             __( 'Optional GPU', 'krita-org-theme' ) . "</strong>" . 
             __( ':', 'krita-org-theme' ) .
             __( 'OpenGL 3.0 or higher', 'krita-org-theme' ) . "<br/><strong>" . 
             __( 'Graphics Tablet Supported', 'krita-org-theme' ) . "</strong>" .
             __( ':', 'krita-org-theme' ) . 
             __( 'Wacom, Huion, Yiyinova, Surface Pro', 'krita-org-theme' ) ;
        ?>

        <div class="download-options">
          <a data-toggle="popover" data-html="true" data-placement="bottom"
             data-content="<?php echo $systemRequirements; ?>">
             <?php esc_html_e( 'System Requirements', 'krita-org-theme' ); ?>

            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               width="20px" height="10px" viewBox="0 0 960 560" enable-background="new 0 0 960 560" xml:space="preserve">
            <g id="down-arrow">
              <path fill="white" d="M480,344.181L268.869,131.889c-15.756-15.859-41.3-15.859-57.054,0c-15.754,15.857-15.754,41.57,0,57.431l237.632,238.937
                c8.395,8.451,19.562,12.254,30.553,11.698c10.993,0.556,22.159-3.247,30.555-11.698l237.631-238.937
                c15.756-15.86,15.756-41.571,0-57.431s-41.299-15.859-57.051,0L480,344.181z"/>
            </g>
            </svg></a>
          <a data-toggle="popover" data-html="true"  data-placement="bottom"
              data-content="

            <a href='<?php echo get_post_meta(592, "Windows 64-bit Installer URL", true)  ?>' >
              <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '64-bit', 'krita-org-theme' ) ?>  (<?php echo $windowssixtyFourInstallerSize . 'MB'; ?>)</a>
            <br/>
            <a href='<?php echo get_post_meta(592, "Windows 32-bit Installer URL", true)  ?>' >
              <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '32-bit', 'krita-org-theme' ) ?>  (<?php echo $windowsthirtyTwoInstallerSize . 'MB'; ?>)</a>
            <br/>
            <a href='<?php echo get_post_meta(592, "Windows 64-bit Portable URL", true)  ?>' >
              <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Portable', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '64-bit', 'krita-org-theme' ) ?>  (<?php echo $windowsSixtyFourPortableSize . 'MB'; ?>)</a>
            <br/>
            <a href='<?php echo get_post_meta(592, "Windows 32-bit Portable URL", true)  ?>' >
              <?php esc_html_e( 'Windows ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Portable', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '32-bit', 'krita-org-theme' ) ?>  (<?php echo $windowsThirtyTwoPortableSize . 'MB'; ?>)</a>
            <br/>
            <a href='<?php echo get_post_meta(592, "OSX DMG URL", true)  ?>' >
              <?php esc_html_e( 'Mac OSX ', 'krita-org-theme' ); ?>  
              <?php esc_html_e( 'Installer', 'krita-org-theme' ); ?> (<?php echo $osxDmgSize . 'MB'; ?>)</a>
            <br/>
            <a class='piwik_download' href='<?php echo get_post_meta(592, "Appimage 64-bit URL", true)  ?>' > 
              <?php esc_html_e( 'Linux', 'krita-org-theme' ); ?> 
              <?php esc_html_e( '64-bit Appimage', 'krita-org-theme' ); ?> (<?php echo $linuxAppImageSize . 'MB'; ?>)</a>
    
            "

          ><?php esc_html_e( 'All Download Versions', 'krita-org-theme' ); ?> <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               width="20px" height="10px" viewBox="0 0 960 560" enable-background="new 0 0 960 560" xml:space="preserve">
            <g id="down-arrow">
              <path fill="white" d="M480,344.181L268.869,131.889c-15.756-15.859-41.3-15.859-57.054,0c-15.754,15.857-15.754,41.57,0,57.431l237.632,238.937
                c8.395,8.451,19.562,12.254,30.553,11.698c10.993,0.556,22.159-3.247,30.555-11.698l237.631-238.937
                c15.756-15.86,15.756-41.571,0-57.431s-41.299-15.859-57.051,0L480,344.181z"/>
            </g>
            </svg></a>
        </div>

      </div>
  </div>   
</div>



<div class="row section-row">
  <div class="col-md-4 fontpage-panel">
    <span class="section-container">
      <h4><?php esc_html_e( 'Windows Store and Steam', 'krita-org-theme' ); ?></h4>
      <a href="https://www.microsoft.com/en-us/store/p/krita/9n6x57zgrw96" target="_blank" class="secondary-button"><?php esc_html_e( 'Windows Store', 'krita-org-theme' ); ?></a>
      <a href="https://store.steampowered.com/app/280680/Krita_Gemini/" target="_blank" class="secondary-button"><?php esc_html_e( 'Steam Store', 'krita-org-theme' ); ?></a>
      <p>
        <?php esc_html_e( 'Paid versions of Krita on other platforms. You will get automatic updates when new versions of Krita come out.', 'krita-org-theme' ); ?>
        <?php esc_html_e('After deduction of the Store fee, the money will support Krita development.', 'krita-org-theme') ?>
        <?php esc_html_e('For the Windows store version you will need Windows 10.', 'krita-org-theme') ?>
        </p>
    </span>
  </div>
    <div class="col-md-4 fontpage-panel">
      <span class="section-container">
        <h4><?php esc_html_e( 'Nightly Builds', 'krita-org-theme' ); ?></h4>
         <p>
          <?php esc_html_e( 'Test out the latest builds that are created every day.', 'krita-org-theme' ); ?>
          </p>

          <ul>
             <li><strong><?php esc_html_e( 'Krita Plus', 'krita-org-theme' ); ?></strong> (<a target="_blank" href="https://binary-factory.kde.org/job/Krita_Stable_Windows_Build/"><?php esc_html_e( 'Windows', 'krita-org-theme' ); ?>
                            </a> |
                            <a target="_blank" href="https://binary-factory.kde.org/job/Krita_Stable_Appimage_Build/">
                              <?php esc_html_e( 'Linux', 'krita-org-theme' ); ?>
                            </a>)
                            -  <?php esc_html_e( 'Daily builds that only contain bug fixes on top of the stable version.', 'krita-org-theme' ); ?>
            </li>
            <li><strong><?php esc_html_e( 'Krita Next', 'krita-org-theme' ); ?></strong> (<a target="_blank" href="https://binary-factory.kde.org/job/Krita_Nightly_Windows_Build/"><?php esc_html_e( 'Windows', 'krita-org-theme' ); ?>
                            </a> |
                            <a target="_blank" href="https://binary-factory.kde.org/job/Krita_Nightly_Appimage_Build/">
                              <?php esc_html_e( 'Linux', 'krita-org-theme' ); ?>
                            </a> | 
                            <a target="_blank" href="https://binary-factory.kde.org/job/Krita_Nightly_MacOS_Build/">
                              <?php esc_html_e( 'OSX', 'krita-org-theme' ); ?>
                            </a>
                            )
                            -  <?php esc_html_e( 'Daily builds that contain new features, but could be unstable.', 'krita-org-theme' ); ?>
            </li>

          </ul>




      </span>
  </div>
    <div class="col-md-4 fontpage-panel">
      <span class="section-container">
        <h4><?php esc_html_e( 'Windows Shell Extension', 'krita-org-theme' ); ?></h4>
        <a href="<?php echo get_post_meta(592, "Windows Shell Extension URL", true)  ?>" class="secondary-button">
          <?php esc_html_e( 'Download', 'krita-org-theme' ); ?> (<?php echo $windowsShellExtensionsSize ?>MB)</a>
        <p>
          <?php esc_html_e( 'The Shell extension is included with the Windows Installer.', 'krita-org-theme' ); ?>  
          <?php esc_html_e( 'An optional add-on for Windows that allow KRA thumbnails to appear in your file browser.', 'krita-org-theme' ); ?>
        </p>
      </span>
  </div>
</div>





<div class="row section-row">
  <div class="col-md-4 fontpage-panel">
    <span class="section-container">
      <h4><?php esc_html_e( 'Source Code', 'krita-org-theme' ); ?></h4>
      <a href="<?php echo get_post_meta(592, "Tarball URL", true)  ?>" class="secondary-button">
        <?php esc_html_e( 'Tarball', 'krita-org-theme' ); ?> (<?php echo $tarballSize  ?>MB)  </a>
      <a href="https://invent.kde.org/kde/krita/tree/master" target="_blank" class="secondary-button"><?php esc_html_e( 'KDE Repository', 'krita-org-theme' ); ?></a>

      <p>
        <?php esc_html_e( 'Krita is a free and open source application.', 'krita-org-theme' ); ?> 
        <?php esc_html_e( 'You are free to study, modify, and distribute Krita under GNU GPL v3 license.', 'krita-org-theme'); ?>
        </p>
    </span>
  </div>
    <div class="col-md-4 fontpage-panel">
      <span class="section-container">
        <h4><?php esc_html_e( 'Download Older Versions', 'krita-org-theme' ); ?></h4>
        <a href="https://download.kde.org/stable/krita/" target="_blank" class="secondary-button">
          <?php esc_html_e( 'Old Version Library', 'krita-org-theme' ); ?>
        </a>
        <p><?php esc_html_e( 'If the newest version is giving you issues there are older versions available for download.', 'krita-org-theme' ); ?></p>
      </span>
  </div>
    <div class="col-md-4 fontpage-panel">
      <span class="section-container">
        <h4><?php esc_html_e( 'GPG Signatures', 'krita-org-theme' ); ?></h4>
        <a href="https://download.kde.org/stable/krita/<?php echo get_post_meta(592, "Version Number", true)    ?>" class="secondary-button"><?php esc_html_e( 'Download Signature', 'krita-org-theme' ); ?></a>
        <p><?php esc_html_e( 'Used to verify the integrity of your downloads. If you don\'t know what GPG is you can ignore it.', 'krita-org-theme' ); ?></p>
      </span>
  </div>
</div>

        <?php // loop isn't used right now
           // php get_template_part('loop', 'index'); ?>


<script>

jQuery( document ).ready( function() {

 jQuery('[data-toggle="popover"]').popover(); 

  // OS detection so it knows what tab to be on
  var OSName="Unknown OS";
  if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
  
  if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";

  if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
  if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
  if (navigator.appVersion.indexOf("Android")!=-1) OSName="Linux";


  if ( OSName == "Windows")
  {      
    document.getElementById("windows-download-information").style.display = "block";

    if (navigator.userAgent.indexOf("WOW64") != -1 || 
        navigator.userAgent.indexOf("Win64") != -1 ) {
        document.getElementById("sixty-four-bit-windows-installer").style.display = "inline-block";
    } else {
        document.getElementById("thirty-two-bit-windows-installer").style.display = "inline-block";
    }

  }
  else if (OSName == "UNIX" || OSName == "Linux")
  {
      document.getElementById("linux-download-information").style.display = "block";
  }
  else if (OSName == "MacOS")
  {
      document.getElementById("osx-download-information").style.display = "block";
  }


  // a download link has been triggered....
  // the "all downloads" area won't trigger this. Because of the popover it isn't working for some reason
  jQuery( ".download-trigger" ).on( "click", function() {
   
      // wait for a couple seconds to make sure it started
      setTimeout( function() {
             // ID should be the page ID for the English Post-Download page
             window.location.href = "<?php echo get_permalink( pll_get_post(7205) )  ?>";
        },
      5000);

  });








  // 3 redirect to the "have fun painting page"
  


}); // end jQuery ready






</script>




        
<?php get_footer();  ?>

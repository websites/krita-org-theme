<?php
	// do_action( 'pre_get_search_form' );

?>

<form role="search" method="get" class="search-form" action="https://www.krita.org/<?php echo pll_current_language() ?>/">
	<label>
		<span class="screen-reader-text"> <?php esc_html_e( 'Search for...', 'krita-org-theme' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php esc_html_e( 'Search for...', 'krita-org-theme' ); ?>"  value="<?php get_search_query() ?>" name="s" title="<?php esc_html_e( 'Search for...', 'krita-org-theme' ); ?>" />
	</label>
	<input type="submit" class="pink-button attached" value="<?php esc_html_e( 'Search', 'krita-org-theme' ); ?>" />
</form>  
	

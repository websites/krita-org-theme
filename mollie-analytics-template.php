<?php
/**
 * Template Name: Mollie Donation History
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0 
 */

?>

<?php get_header(); ?>

<div class="row">
    <div class="content-container col-md-12" id="content-<?php the_ID(); ?>" >
        <?php get_template_part('loop', 'index'); ?>

  <h2>Donation Amounts per month</h2>
  <div style="width:100%; margin-bottom: 10rem" >
    <canvas id="donationsPerMonth"></canvas>
  </div>


  <h2>Donation Amounts per day</h2>
  <div style="width:100%; margin-bottom: 10rem" >
    <canvas id="myChart"></canvas>
  </div>

  <h2>Donation Amounts per week</h2>
  <div style="width:100%; margin-bottom: 10rem" >
    <canvas id="donationsPerWeek"></canvas>
  </div>

  <h2>Donation Transactions per day</h2>
  <div style="width:100%;">
    <canvas id="donationTransactionsChart"></canvas>
  </div>


    </div>   
</div> <!-- end row -->    


<?php

  if (!is_page())
  { ?>
  
  <div class="row">
            <div class="col-md-6 content-container">
    <?php get_template_part( 'email-signup-snippet' ); ?>
      </div>
  </div>

<?php  } ?>



<?php 

// SHOW TABLES shows these exist
// wp_mollie_forms_customers
// wp_mollie_forms_payments
// wp_mollie_forms_price_options
// wp_mollie_forms_registration_fields
// wp_mollie_forms_registration_price_options
// wp_mollie_forms_registrations
// wp_mollie_forms_subscription


// COLUMNS FOR wp_mollie_forms_payments
// id mediumint(9) NO PRI auto_increment 
// created_at datetime NO 
// registration_id mediumint(9) NO 
// payment_id varchar(45) NO 
// payment_method varchar(255) NO 
// payment_mode varchar(255) NO 
// payment_status varchar(255) NO 
// currency varchar(45) YES 
// amount decimal(8,2) NO 
// rfmp_id varchar(255) NO 


// COLUMNS for wp_mollie_forms_registrations
// id mediumint(9) NO PRI auto_increment 
// created_at datetime NO 
// post_id mediumint(9) NO 
// customer_id varchar(45) YES 
// subscription_id varchar(45) YES 
// currency varchar(45) YES 
// total_price decimal(8,2) YES 
// total_vat decimal(8,2) YES 
// vat_setting varchar(45) YES 
// price_frequency varchar(45) YES 
// number_of_times mediumint(9) YES 
// description varchar(255) YES 
// subs_fix smallint(1) NO 0 


// this snippet of code tells us all the tables that exist in the database
// not needed, but helpful if it is hard to get access to mysql database visualizer
// $mytables = $wpdb->get_results("SHOW TABLES");
// foreach ($mytables as $mytable)
// {
//     foreach ($mytable as $t) 
//     {       
//         echo $t . "<br>";
//     }
// }


// this grabs all the columns that exist for the table
// $columnNames =  $wpdb->get_results( "SHOW COLUMNS FROM wp_mollie_forms_registrations", ARRAY_N  ); 

// if ($columnNames) {
//   foreach ( $columnNames as $column ) 
//    {
//       foreach ( $column as $prop ) 
//      { 
//         echo $prop . " ";
//      }
//      echo "<br/>";

//    }
// }


global $wpdb;

// $results = $wpdb->get_results( "SELECT * FROM { getRegistrationsTable() }", OBJECT );
$results = $wpdb->get_results( "SELECT created_at, payment_method, payment_status, currency, amount FROM wp_mollie_forms_payments", ARRAY_N  );
// if ($results) {
//    foreach ( $results as $payments ) 
//    {
//       $regDate = date("d-m-Y", strtotime($payments[1]) );
//       echo "Donation Date: " . $regDate . 
//       " Payment Type:" .  $payments[4] .
//       " Payment Status:" .  $payments[6] .
//       " Amount:" . $payments[7]  .  " " . $payments[8] . "<br/>";    
//    }
// } else {
//   // echo "<p> no registrations. </p>" . getRegistrationsTable() ;
// }


print "<script>jsonData=" . json_encode($results) . ";</script>" ;

?>

  <script src="<?php bloginfo('stylesheet_url'); ?>/../js/chart.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js" ></script>
  <script src="<?php bloginfo('stylesheet_url'); ?>/../js/mollie-analytics-scripts.js?v190413"></script>


<?php get_footer();  ?>

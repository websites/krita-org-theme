<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <title>
            <?php
            // Print the <title> tag based on what is being viewed.
            global $page, $paged;
        
            wp_title( '|', true, 'right' );
        
            // Add the blog name.
            bloginfo( 'name' );
        
            // Add the blog description for the home/front page.
            $site_description = get_bloginfo( 'description', 'display' );
            if ( $site_description && ( is_home() || is_front_page() ) )	echo " | $site_description";
                
            ?>
        </title>
        
        <meta charset="<?php echo bloginfo('charset'); ?>"  />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Krita.org">
        <meta name="keywords" content="Krita, drawing, painting, concept, art">
        <meta name="author" content="Krita Foundation">

	<!-- needed to translate the site into other languages -->
	<meta name="google-translate-customization" content="4bc3be5ef586010a-7af5cf77f24712d6-gc36cc55fc66d6ffc-a"></meta>


  	<link href="<?php echo bloginfo('template_directory')?>/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo bloginfo('template_directory')?>/style.css?v=190902-01">
       
       <link href='https://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
       <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700' rel='stylesheet' type='text/css'> 



       <?php  $currentLanguage = substr(get_bloginfo( 'language' ), 0, 2)   ?>
       <?php if ($currentLanguage == "ja"): ?>

           <link href='//fonts.googleapis.com/earlyaccess/notosansjapanese.css' rel='stylesheet' type='text/css'> 
           <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.5.2' type='text/css' media='all' />
           <link rel='stylesheet'  href='<?php echo bloginfo('template_directory')?>/css/locale/japan.css' type='text/css' media='all' />
       <?php endif ?>




        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
            
         <?php wp_head(); // plug ins and enqueue_scripts need this to latch on to   ?>
              
    </head>
    <body>
 


<!--
<div id="countdown" style="text-align: center; background-color: #333;">
    <a href="https://krita.org/en/fundraising-2018-campaign/" target="_self" onclick="ga('send', 'event', 'frontpage', 'button', 'Fundraiser 2018');" >
        <img src="<?php echo bloginfo('template_directory')?>/images/decoration/2018-fundraiser-banner.png" style="max-width: 100%" />
    </a>
</div>

-->




    <!-- Mollie donation header -->
    <?php get_template_part( 'mollie-header-monthly-donations' ); ?> 







<!-- // this will be a countdown that spans the entire width with a pink banner
<div id="countdown">
	<p><span id="pageTimer"></span> left</p>
</div> -->


<script>


jQuery(document).ready(function(){



// new Date ( year, month, day, hours, minutes, seconds, milliseconds);
// note that the month is 0 index (ie February is 1)

/*
var releaseDate = new Date(2015, 10, 5, 5, 0, 0, 0);

var timerId =
  countdown(
    releaseDate,
    function(ts) {
      document.getElementById('pageTimer').innerHTML =  ts.toHTML("strong");
    },
    countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
*/
// later on this timer may be stopped
//window.clearInterval(timerId);
// see countdown.js for more information on options



});



</script>

<!--
<div id="countdown" style="text-align: center; background-color: #333;"><a href="https://www.kickstarter.com/projects/krita/krita-free-paint-app-lets-make-it-faster-than-phot" target="_blank" onclick="ga('send', 'event', 'frontpage', 'button', 'Kickstarter banner');" ><img src="https://krita.org/wp-content/uploads/2015/05/support-thin.png" style="max-width: 100%" /></a></div>



 -->





    
        <!--navigation -->
        <div class="navbar <?php  if ( is_home() || is_page_template( 'krita-homepage.php' ) )  { echo 'homepage'; }  ?>" role="navigation">
         
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand mobile-nav-logo" href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_directory')?>/images/krita-logo.png" alt="Krita Logo" /></a>
            </div>
            <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav">
                <li class="hidden-xs">
                    <a href="<?php echo get_bloginfo('url'); ?>" class="brandingPadding">
                    <img src="<?php echo bloginfo('template_directory')?>/images/krita-logo.png" alt="Krita Logo" /></a>
                </li>
                <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav navbar-nav' )); ?>
            
                <?php if ( is_active_sidebar( 'main_menu_lang' ) ) : ?>            
                    <li><div id="lang-switcher-widget"> <?php dynamic_sidebar( 'main_menu_lang' ); ?> </div></li>
                <?php endif; ?>
            </ul>

            
            </div><!--/.nav-collapse -->
        </div>
          
        <!-- main content -->
        
        <div class="container" style="opacity: 0.99" > <!-- 0.99 opacity bug fix for Safari on OSX (Sierra). has to do with z-order stack -->
            
             <?php // only show the slim background and navigation on internal pages (not homepage)             
                if ( !is_home() && 
                     !is_page_template( 'krita-desktop.php' ) && 
                     !is_page_template( 'krita-homepage.php' ) && 
                     !is_page_template( 'krita-post-download.php' )  )
                {       
    
             ?>
            


                <!-- slim area for header and breadcrumb navigation -->
                <div class="row">
                    <div id="slim-background" class="col-md-12" ></div>                    
                </div>

                <div class="row hidden-xs hidden-sm internal" id="header-decoration" >
                    <div class="col-md-12">&nbsp;</div>
                </div>


<!-- INSIDE GLOBAL BANNER HERE 


                <div class="row hidden-xs " style="text-align: center; background-color: #567;">
                    <div class="col-md-12 global-top-banner" onclick="window.location.href='https://www.indiegogo.com/projects/pepper-and-carrot-motion-comic/'; _paq.push(['trackEvent', 'FrontPage', 'button', 'comic-graphic-novel']); "></div>      
                   <div class="col-md-12" style="height: 2em; background: white"></div>              
                </div>

 END INSIDE GLOBAL BANNER -->


            <?php } // end is_home() ?>


             <?php 
             // only show the slim background and navigation on internal pages (not homepage)             
             // also do not show if there is no parent. that means no breadcrumbs
             

                if ( is_home() || 
                     is_page_template( 'krita-homepage.php' ) ||
                     is_page_template( 'krita-desktop.php' ) || 
                     is_page_template( 'krita-post-download.php' ) || 
                     is_page() &&  empty( $post->post_parent ) ) // page doesn't have any hierarchy
                {    
                  // these situations do NOT show breadcrumbs
                }
                else {


    
             ?>


                <div id="breadcrumb-nav" class="row">

                <p id="breadcrumb-title" ><?php 
                        
                            if ( is_404() )
                            {
                                echo   esc_html_e( 'Page Not Found', 'krita-org-theme' ); 
                            }
                            else if ( is_search() )
                            {
                                echo esc_html_e( 'Search Results', 'krita-org-theme' );
                            }
                            else
                            {
                                // this echo statement shows the parent page. hiding for now
                                // echo empty( $post->post_parent ) ? '' : get_the_title( $post->post_parent ) . " "; 

                                // once had a > symbol after the breadcrumb container text
                            }
                        

                        
                        ?></p>               


                    <ul>                
                        <?php
                        // if the page's parent doesn't have children, we shouldn't be showing a sub nav
                         // Set up the WP query to get needed
                        $my_wp_query = new WP_Query();
			$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order'));              
                        
                         // get children of container so we know all of the sections that need to display
                         // logic: to have a sub menu, the page MUST have a parent to group everything.
                         // if the parent is 0, that means it is at the top level. we don't want to show
                         // a submenu if it is top level element

                        $container_page = $post->post_parent;
                        $hasParent = $post->post_parent != 0;
                        $children_pages = get_page_children($container_page, $all_wp_pages );
                         
			// prints out array of children ( currently is empty for one that doesn't work)  
			//echo print_r( $children_pages, true ) ;
    
                        if (is_page() && $hasParent )
                        {             
	
                            // generate the DOM
                            foreach ($children_pages as $key=>$wpPostObject) {
                
                                $className = ''; 
                                if ( $wpPostObject->ID == $post->ID)
                                    $className = 'active';
                                
                                
                                echo '<li class="' . $className  .  '" ><a href="' . get_permalink( $wpPostObject->ID ) . '">' . $wpPostObject->post_title . '</a></li>';
                            }
                            
                            // echo results we get back from WP to the browser
                            //echo '<pre>' . print_r( $children_pages, true ) . '</pre>';
                        }
                        else
                        {
                            echo '<li></li>'; // this will keep the blue bar
                        }
                       ?>                 
                    </ul>
                </div>

                  
            <?php } // end showing breadcrumbs logic ?>
          
                


                    <div class="post page">
                    <?php
                    //starting "the loop" to see what type of posts/pages will come back
                    if (have_posts() ) 
                    {
                        $loopIndex = 0; // counter since WordPress doesn't have one for post index
                            
                        while (have_posts())
                        {        
                            the_post(); // iterate the post index in "The Loop". Retrieves next post, set up the post

                            
                            //If there is just a single post or page(still single) 
                            //only show comments on single posts/pages.
                            if ( is_single() || is_page())
                            {
                                get_template_part( 'content-single' ); 
                            }            
                            else
                            {
                                // if there are multiple posts
                                // using this retains variables outside of scope (for $loopIndex)
                                include(locate_template('content-multiple.php')); 
                                $loopIndex++;
                            }
                        }
                    }
                    else
                    {
                        //there are no posts/content to display
                         get_template_part( 'content-none' );
                    }
                    ?>

                    </div>

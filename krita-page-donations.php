<?php
/**
 * Template Name: Krita Page - Donation area
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>
<?php get_header(); ?>
<div class="row" id="donation-hero-image" >
	<div class="col-md-12">
	</div> 
</div>

<div class="row" id="donation-hero-caption" >
	<div class="col-md-12">
		2019 Krita Contributor Meeting  - Deventer, Netherlands
	</div> 
</div>


<!-- donation template -->
<div class="row" style="margin-bottom: 4em">
    <div class="content-container col-md-8" id="content-<?php the_ID(); ?>" style="padding-top: 0" >
        <?php get_template_part('loop', 'index'); ?>
    </div>   

    <div class="col-md-3">
      <div>
          <?php
          global $wp_query;
          $postid = $wp_query->post->ID;
          echo do_shortcode( get_post_meta($postid, 'donation-right-1', true) ); // allows for shortcodes
          wp_reset_query();
          ?>
      </div>


      <div style="margin-top: 3em" class="donation-sponsors-area">
          <?php
          global $wp_query;
          $postid = $wp_query->post->ID;
           echo do_shortcode( get_post_meta($postid, 'donation-right-2', true) ); // allows for shortcodes
          wp_reset_query();
          ?>
      </div>



    </div>

</div> <!-- end row -->    








<?php

  if (!is_page())
  { ?>
  
  <div class="row">
            <div class="col-md-6 content-container">
    <?php get_template_part( 'email-signup-snippet' ); ?>
      </div>
  </div>

<?php  } ?>

<style>

.donation-sponsors-area img {
  width: 100%;
}

#donation-hero-image {
	background: url('https://krita.org/wp-content/uploads/2019/09/krita-team-donation.jpg'); 
	margin: 0 -15px; 
	height: 34rem; 
	background-repeat: none;
	background-size: cover; 
	position: relative; 
	top: -14rem; 
	z-index: -9999; 
	margin-bottom: -11rem;
}

@media (max-width: 1000px) {
	#donation-hero-image {
		margin: 0;
		top: 0;
		z-index: 0;
		margin-bottom: 0;
		height: 17rem;
	}
}

#donation-hero-caption {
	position: relative;
	top: -6rem;
	margin-left: 3rem;
	color: white;
	text-shadow: 3px 3px 3px black;
	font-weight: bold;
	height: 0;

}
#donation-hero-caption div {
		line-height: 129%;
}
@media (max-width: 1000px) {
	#donation-hero-image {
		top: -3rem;
		margin-left: 1rem;
	}
}

</style>


                 
<?php get_footer();  ?>



<?php
/**
 * Template Name: Krita Page 75% Width
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>


<?php get_header(); ?>

<div class="row">
    <div class="content-container col-md-8" id="content-<?php the_ID(); ?>" >
        <?php get_template_part('loop', 'index'); ?>
    </div>   
</div> <!-- end row -->    





<?php

  if (!is_page())
  { ?>
  
  <div class="row">
            <div class="col-md-6 content-container">
    <?php get_template_part( 'email-signup-snippet' ); ?>
      </div>
  </div>

<?php  } ?>

                 
<?php get_footer();  ?>

<?php
/**
 * Template Name: Gallery Page
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>




<?php get_header(); ?>

<div class="row">
    <div class="content-container col-md-12" id="content-<?php the_ID(); ?>" >
        <?php get_template_part('loop', 'index'); ?>
    </div>   
</div> <!-- end row -->    





<?php

	if (!is_page())
	{ ?>
	
	<div class="row">
            <div class="col-md-6 content-container">
		<?php get_template_part( 'email-signup-snippet' ); ?>
	    </div>
	</div>

<?php  } ?>


<style>
.masonryImage {
    position: relative;
}

.masonryImage span {
color: white;
    position: absolute;
bottom: 0;
right: 0;
width: 100%;
text-align: center;
background-color: black;
padding: 0.5em;
padding-bottom: 0.3em;

background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(3%,rgba(0,0,0,0)), color-stop(73%,rgba(0,0,0,0.64)), color-stop(74%,rgba(0,0,0,0.65)));
background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 3%,rgba(0,0,0,0.64) 73%,rgba(0,0,0,0.65) 74%);
background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 3%,rgba(0,0,0,0.64) 73%,rgba(0,0,0,0.65) 74%);
background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 3%,rgba(0,0,0,0.64) 73%,rgba(0,0,0,0.65) 74%);
background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 3%,rgba(0,0,0,0.64) 73%,rgba(0,0,0,0.65) 74%);

}

.masonryImage img {
 opacity: 1.0f;
        transition: all 0.3s ease;
    border: 0;
    width: 430px;
    height: auto;
}

.masonryImage img:hover {
    opacity: 0.8;
    border: 0.1em solid #ccc;
}


/* have a few breakpoints with 3 columns */

@media (max-width: 1450px) {
    
    .masonryImage img {
        width: 360px;
    }
}


@media (max-width: 1250px) {
    
    .masonryImage img {
        width: 330px;
    }
}


@media (max-width: 1150px) {
    
    .masonryImage img {
        width: 300px;
    }
}



@media (max-width: 768px) {
#gallery-container{
    margin: 0;
}

@media (max-width: 400px) {
    .masonryImage img {
        width: 320px;
display:block;
    }
}



}

</style>

<div id="loadingText" class="row">
    <div class="col-md-12">
        <h2 style="text-align: center"><?php esc_html__( 'loading... ', 'krita-org-theme' ); ?></h2>
    </div>  

</div>


<div id="gallery-container" clas="row">
    <div id="container">
        

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ComamitsuZaki-full-web.jpg">           
                <img class="horizontal" src="<?php echo bloginfo('template_directory') ?>/images/gallery/ComamitsuZaki-full-web.jpg" alt="" />
                <span>Girl to protect the sleep - ComamitsuZaki（狛蜜ザキ）</span>
            </a>
            
        </div>


        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ride_with_pride_by_chairilandri-d7gne60.jpg">          
                <img class="horizontal" src="<?php echo bloginfo('template_directory') ?>/images/gallery/ride_with_pride_by_chairilandri-d7gne60.jpg" alt="" />
                <span>Ride with Pride - chairilandri</span>
            </a>
            
        </div>


        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/a_cute_fantasy_dragon_sheep_by_deevad-d7a5iq4.jpg">            
                <img class="x-vertical" src="<?php echo bloginfo('template_directory') ?>/images/gallery/a_cute_fantasy_dragon_sheep_by_deevad-d7a5iq4.jpg" alt="" />
                <span>Fantasy Dragon Sheep - David Revoy</span>
            </a>
            
        </div>


        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/alex-sabo.jpg">            
                <img class="x-vertical" src="<?php echo bloginfo('template_directory') ?>/images/gallery/alex-sabo.jpg" alt="" />
                <span>Elf Warrior - Alexandru Sabo</span>
            </a>
            
        </div>





        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/krita_test___self_portrait_by_sycra-d6rang3.jpg">          
                <img class="x-vertical" src="<?php echo bloginfo('template_directory') ?>/images/gallery/krita_test___self_portrait_by_sycra-d6rang3.jpg" alt="" />
                <span>Self Portrait - Sycra</span>
            </a>
            
        </div>





        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/jason_statham__cartoon_caricature__by_wilson_santos-d7h61bg.png">          
                <img class="vertical" src="<?php echo bloginfo('template_directory') ?>/images/gallery/jason_statham__cartoon_caricature__by_wilson_santos-d7h61bg.png" alt="" />
                <span>Jason S. - Wilson Santos</span>
            </a>
            
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/cranesicono.jpg">      
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/cranesicono.jpg" alt="" />
                <span>Cranes - Namito</span>
            </a>
            
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/healing_heart_by_ico_dy-d5umki0.jpg">  
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/healing_heart_by_ico_dy-d5umki0.jpg" alt="" />
                <span>Healing Hearts - Enrico Guarnieri</span>
            </a>
        </div>


        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/KaterynaHerasymenko.jpg">
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/KaterynaHerasymenko.jpg" alt="" />
                <span>Wasteland(Magic) - Kateryna Herasymenko</span>
            </a>
        </div>  

        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ink_girl_by_ico_dy-d62vdsc.jpg">               
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/ink_girl_by_ico_dy-d62vdsc.jpg" alt="" />
                <span>Ink Girl - Enrico Guarnieri</span>
            </a>
        </div>
        

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/LAdy Night_speedpainticono.jpg">   
                <img class="vertical" src="<?php echo bloginfo('template_directory')?>/images/gallery/LAdy Night_speedpainticono.jpg" alt="" />
                <span>Speed Paint - Ramón Miranda</span>
            </a>
        </div>

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/butterfly_shout_by_ico_dy-d53me1n.jpg">            
                <img class="horizontal" src="<?php echo bloginfo('template_directory') ?>/images/gallery/butterfly_shout_by_ico_dy-d53me1n.jpg" alt="" />
                <span>Butterfly Shout - Enrico Guarnieri</span>
            </a>
            
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/les_miserables_by_tago73-d5xko2n.jpg"> 
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/les_miserables_by_tago73-d5xko2n.jpg" alt="" />
                <span>Les Miserable - Tago</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/mjornicon.jpg">    
                <img class="xx-horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/mjornicon.jpg" alt="" />
                <span>mjorn - Jessica Mars</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/mouse_by_coyau-d5qwt9ricon.jpg">   
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/mouse_by_coyau-d5qwt9ricon.jpg" alt="" />
                <span>Mouse - coyau</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/Nayobe.jpg">   
                <img  class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/Nayobe.jpg" alt="" />
                <span>Nayobe Mills</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/oliver_twist_by_tago73-d3nqfov.jpg">
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/oliver_twist_by_tago73-d3nqfov.jpg" alt="" />
                <span>Oliver Twist - Tago</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/The birth of Nanths_final highresicono.jpg">
                <img class="xx-horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/The birth of Nanths_final highresicono.jpg" alt="" />
                <span>The birth of Nanths - Ramón Miranda</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/thewarbegins_by_endoraniendo-d5vlzk0.jpg">
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/thewarbegins_by_endoraniendo-d5vlzk0.jpg" alt="" />
                <span>The War Begins - endoraniendo</span>
            </a>
        </div>
        
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/Unfinished 1icono.jpg">
                <img class="x-vertical" src="<?php echo bloginfo('template_directory')?>/images/gallery/Unfinished 1icono.jpg" alt="" />
                <span>Unfinished - Yuri Fidelis</span>
            </a>
        </div>  

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ksenia.jpg">
                <img class="x-vertical" src="<?php echo bloginfo('template_directory')?>/images/gallery/ksenia.jpg" alt="" />
                <span>ksenia</span>
            </a>
        </div>  

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ericlee-tiger.jpg">
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/ericlee-tiger.jpg" alt="" />
                <span>Tiger - Eric Lee</span>
            </a>
        </div>  

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/grumpy_cat_by_peileppe-d5g98qv.jpg">
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/grumpy_cat_by_peileppe-d5g98qv.jpg" alt="" />
                <span>Grumpy Cat - Peileppe</span>
            </a>
        </div>  

        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/Metamorphosis-Enrico.jpg">
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/Metamorphosis-Enrico.jpg" alt="" />
                <span>Metamorphosis - Enrico Guarnieri</span>
            </a>
        </div>  
    
        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/master_and_apprentice_by_endoraniendo-d5yyf17.jpg">
                <img class="horizontal" src="<?php echo bloginfo('template_directory')?>/images/gallery/master_and_apprentice_by_endoraniendo-d5yyf17.jpg" alt="" />
                <span>Master &amp; Apprentice - endoraniendo</span>
            </a>
        </div>  


        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/ericlee-fighter.jpg">
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/ericlee-fighter.jpg" alt="" />
                <span>Figher - Eric Lee</span>
            </a>
        </div>  



        <div class="masonryImage">
            <a href="<?php echo bloginfo('template_directory') ?>/images/gallery/hermitage.jpg">
                <img class="square" src="<?php echo bloginfo('template_directory')?>/images/gallery/hermitage.jpg" alt="" />
                <span>Hermitage - Paweł Majewski</span>
            </a>
        </div>  




    </div>
</div>




  
         
<!-- Supersized slideshow jQuery plug in -->
<!-- only load these on templates that use the slideshow plug in (gallery)   -->
<script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/jquery.masonry.min.js"></script>  
<!--<link href="<?php echo bloginfo('template_directory')?>/css/galleriffic-5.css" rel="stylesheet"> -->
          
<script>
jQuery(document).ready(function(){
       jQuery("#gallery-container").hide();
       var $container = jQuery('#container');
     
    


       $container.imagesLoaded( function(){
        
        jQuery("#gallery-container").show();
        jQuery("#loadingText").hide();

    $container.masonry({
           itemSelector : '.masonryImage'
          
         });

    

       }).fadeIn();


    // this probably won't be used except for web designers
    // testing the 'responsive' nature of it
    jQuery( window ).resize(function() {    

            

        $container.masonry('reloadItems');
            
        try 
        {
            $container.masonry().layout();
        }
        catch(e)
        {           
        }

    });



});
</script>


         
        
<?php get_footer(); ?>

<?php
/**
 * Template Name: News Page
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>





<?php get_header(); ?>

<div class="row">
    <div class="content-container col-md-12" id="content-<?php the_ID(); ?>" >
        <?php get_template_part('loop', 'index'); ?>
    </div>   
</div> <!-- end row -->    





<?php

	if (!is_page())
	{ ?>
	
	<div class="row">
            <div class="col-md-6 content-container">
		<?php get_template_part( 'email-signup-snippet' ); ?>
	    </div>
	</div>

<?php  } ?>


      

<div class="row content-container">



<div class="col-md-12" id="searchResults-nav" >

            <?php if ( have_posts() ) : ?>
                <span id="searchResultsLocation"> <?php esc_html_e( 'Showing All News', 'krita-org-theme' ); ?></span>
        <?php endif; ?>

    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
            <label class="hidden" for="s"></label>
            <label>Refine Search</label>
            <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Search" />
            <input type="submit" id="searchsubmit" value="Go" />                
        </form>
    

</div>      

</div>


<div class="row">
    <div class="content-container col-md-8">
    
    
    


            <?php
            
            // create a new WP_Query that gets the latest news (20)
            global $posts_Query;
               
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            
            $queryArgs = array(
                'post_type' => 'any',
                'post_status' => 'publish',
                'posts_per_page' => 20,
                'paged' => $paged
            );
           $posts_Query = new WP_Query($queryArgs);
            
            if (  $posts_Query->have_posts() ) 
            {
                while ( $posts_Query->have_posts() ) 
                {
                    $posts_Query->the_post();

                    //set up date formatting
                    $full_date = get_the_date( $d );                
                ?>

                <div class="post-excerpt">
                    <div class="excerpt-date"><?php echo the_time('M') ?>
            <span><?php echo the_time('j') ?></span>
            <span style="font-size: 16px;"><?php echo the_time('Y') ?></span>
            </div>

                    <div class="excerpt-content">
                        <a href="<?php echo get_permalink();  ?>"><?php echo the_title()  ?></a>
                        
                        <?php // limit the characters of the excerpt to 300 and add an ellipsis to the end (looks more condensed this way)  ?>
                        <div><?php  echo substr(get_the_excerpt(), 0,300) . '...'; ?></div>
                    </div>
                </div>  
                    
                    
                 <?php   
                }
            }
            ?>           
            
            
    </div>
    
    
</div>

<div class="row">
    <div class="pagination col-md-12 content-container">
    <?php

    $big = 999999999; // need an unlikely integer

    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' =>  $posts_Query->max_num_pages
    ) );
    ?> 

    </div>
</div>







         
        
<?php get_footer(); ?>

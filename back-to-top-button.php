 <button onclick="topFunction()" class="pink-button" id="myBtn" title="Go to top"><?php esc_html_e( 'Top', 'krita-org-theme' ); ?></button> 

<style>
#myBtn {
  display: none; /* Hidden by default */
  position: fixed; /* Fixed/sticky position */
  bottom: 77px; /* Place the button at the bottom of the page */
  right: 30px; /* Place the button 30px from the right */
  z-index: 99; /* Make sure it does not overlap */
  cursor: pointer; /* Add a mouse pointer on hover */
  padding: 10px; /* Some padding */
  border-radius: 10px; /* Rounded corners */
}


</style>


<script>
// When the user scrolls down from the top of the document, show the button
var startScrollingAtPosition = 1000;

window.onscroll = function() {scrollFunction()};


function scrollFunction() {
  if (document.body.scrollTop > startScrollingAtPosition || document.documentElement.scrollTop > startScrollingAtPosition) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
</script>
